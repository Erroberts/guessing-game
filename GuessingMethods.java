package Phase1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

class GuessingMethods {


    //  private void listCreator(int ){


    GuessingMethods() {
        int count = 0;
        String letterGuessed;
        Random choice = new Random();
        Scanner userInput = new Scanner(System.in);
        List<String> listOfGuessedLetter = new ArrayList<>();
        List<String> wrongLettersGuessed = new ArrayList<>();
        List<String> listOfPossibleAnswers = new ArrayList<>();
        boolean end = false;
        int limit;
        String lettersGuessedCorrectly = "";
        String wordGuess;
        listOfPossibleAnswers.add("computer");
        listOfPossibleAnswers.add("logitech");
        listOfPossibleAnswers.add("python");
        listOfPossibleAnswers.add("javascript");
        listOfPossibleAnswers.add("watermelon");
        String correctAnswer = listOfPossibleAnswers.get(choice.nextInt(listOfPossibleAnswers.size()));

        System.out.println("Your first word is: " + correctAnswer.length() + " long");
        System.out.println(("You have: " + (correctAnswer.length() - 2) + " guesses to get the right answer."));


        while (!end) {
            limit = correctAnswer.length() - 2;
            System.out.println("you have: " + limit + " Guesses out of " + count + " left. Guess a letter");
            letterGuessed = userInput.next();

            if (listOfGuessedLetter.contains(letterGuessed)) {
                System.out.println("you have already guessed this letter.");
            } else if (correctAnswer.contains(letterGuessed) && !listOfGuessedLetter.contains(letterGuessed)) {
                System.out.println("You have guessed a correct letter.");
                listOfGuessedLetter.add(letterGuessed);
            } else {
                System.out.println("Your guess was incorrect. Try again");
                wrongLettersGuessed.add(letterGuessed);
            }
            count++;
            if (count == 2 && listOfGuessedLetter.size() < 2) {
                System.out.println("Would you like a hint:" + '\n' + "type y for 'YES' and n for  'NO'.");
                if (userInput.next().equalsIgnoreCase("y")) {
                    System.out.println("This word deals with Technology.");
                } else {
                    System.out.println("Oh, so you must think you are smart.Well play on player!!");
                }
            }
            if (count == 4 && listOfGuessedLetter.size() < 4) {
                System.out.println("Would you like another hint:" + '\n' + "type y for 'YES' and n for  'NO'.");
                if (userInput.next().equalsIgnoreCase("y")) {
                    System.out.println("This is hint #2.");
                }
            }

            if (count == limit - 1) {
                System.out.println("This is your finally guess. You have guessed " + listOfGuessedLetter.size() + "letters correctly.");
                for (String i : listOfGuessedLetter) {
                    lettersGuessedCorrectly += i;
                }
                System.out.println("These are the letters you guessed incorrectly: " + wrongLettersGuessed);
                System.out.println("Here are the letter correct :" + lettersGuessedCorrectly);
                System.out.println("Remember the word is " + correctAnswer.length() + " long.");
                wordGuess = userInput.next();
                if (wordGuess.equalsIgnoreCase(correctAnswer)) {
                    System.out.println("You guessed it !! Great job");
                    end = true;
                } else {
                    System.out.println("You did not guess the correct answer. it was " + correctAnswer + ". Please try again.");

                }
            }


        }
    }
}




